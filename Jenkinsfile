pipeline {
  environment {
    ARGO_SERVER = '34.131.127.26:32100'
    DEV_URL = 'http://34.131.127.26:30080/'
   }
  agent {
    kubernetes {
      yamlFile 'build-agent.yaml'
      defaultContainer 'maven'
      idleMinutes 1
    }
  }
  stages {
    stage('Repo Scan') {
      parallel {
        stage('Secrets Scanner') {
          steps {
            container('trufflehog') {
              sh 'git clone ${GIT_URL}'
              sh 'cd dso-demo && ls -al'
              sh 'cd dso-demo && trufflehog --entropy=False .'       
        }
       }
      }
     }
    }
    stage('Build') {
      parallel {
        stage('Compile') {
          steps {
            container('maven') {
              sh 'mvn compile'
            }
          }
        }
      }
    }
    stage('Static Analysis') {
      parallel {
        stage('Unit Tests') {
          steps {
            container('maven') {
              sh 'mvn test'
            }
          }
        }
        stage('SCA') {
          steps {
            container('maven') {
              catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                sh 'mvn org.owasp:dependency-check-maven:check'
                }
              }
            }
          post {
            always {
              archiveArtifacts allowEmptyArchive: true, artifacts: 'target/dependency-check-report.html', fingerprint: true, onlyIfSuccessful: true
              // dependencyCheckPublisher pattern: 'report.xml'
              }
            }
           }
        stage('Generate SBOM') {
          steps {
            container('maven') {
              sh 'mvn org.cyclonedx:cyclonedx-maven-plugin:makeAggregateBom'
            }
          }
          post {
            success {
              dependencyTrackPublisher projectName: 'sample-spring-app', projectVersion: '0.0.1', artifact: 'target/bom.xml', autoCreateProjects: true, synchronous: true
              archiveArtifacts allowEmptyArchive: true, artifacts: 'target/bom.xml', fingerprint: true, onlyIfSuccessful: true
            }
          }
        }
        stage('OSS License Checker') {
          steps {
            container('licensefinder') {
              catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                  sh 'ls -al'
                  sh '''#!/bin/bash --login
                        /bin/bash --login
                        rvm use default
                        gem install license_finder
                        license_finder
                        '''
                      }
                  }
              }
          }
        stage('SAST') {
          steps {
            container('slscan') {
              sh 'scan --type java,depscan --build'
              }
            }
            post {
              success {
                archiveArtifacts allowEmptyArchive: true, artifacts: 'reports/*', fingerprint: true, onlyIfSuccessful: true
              }
            }
          }
      }
    }
    stage('Snyk Testing') {
      steps {
        withCredentials([string(credentialsId: 'synk_api_token', variable: 'snyk_token')]) {
          container('snyk-cli') {
            sh 'snyk auth ${snyk_token}'
            sh 'snyk test --org=patrasingh0811'
            }
        }
      }
    }
    stage('Package') {
      parallel {
        stage('Create Jarfile') {
          steps {
            container('maven') {
              sh 'mvn package -DskipTests'
            }
          }
        }
          stage('Docker BnP') {
            steps {
              container('kaniko') {
                sh '/kaniko/executor -f `pwd`/Dockerfile -c `pwd` --insecure --skip-tls-verify --cache=true --destination=docker.io/kushwap1/dsodemo'
              }
            }
         }
      }
    }
    stage('Image Analysis') {
      parallel {
        stage('Image Linting') {
          steps {
            container('docker-tools') {
              sh 'dockle docker.io/kushwap1/dsodemo'
            }
          }
        }
        stage('Image Scan') {
          steps {
            container('docker-tools') {
              sh 'trivy image --exit-code 0 kushwap1/dsodemo'
            }
          }
        }
      }
     }
    stage('Deploy to Dev') {
      environment {
        AUTH_TOKEN = credentials('argocd-jenkins-deployer-token')
      }
      steps {
        container('docker-tools') {
          sh 'docker run -t schoolofdevops/argocd-cli argocd app sync dsodemo --insecure --server $ARGO_SERVER --auth-token $AUTH_TOKEN'
          sh 'docker run -t schoolofdevops/argocd-cli argocd app wait dsodemo --health --timeout 300 --insecure --server $ARGO_SERVER --auth-token $AUTH_TOKEN'
        }
       }
     }
    stage('Dynamic Analysis') {
      parallel {
        stage('E2E tests') {
          steps {
            sh 'echo "Funtional Tests are passed !!!"'
          }
         }
        stage('DAST') {
          steps {
            container('docker-tools') {
              sh 'docker run -t owasp/zap2docker-stable zap-baseline.py -t $DEV_URL || exit 0'
            }
           }
         }
        stage('StackHawk DAST') {
          environment {
            HAWK_API_KEY = credentials('HAWK_API_KEY')
          }
          steps {
            container('docker-tool') {
              sh 'docker run -v ${WORKSPACE}:/hawk:rw -t -e API_KEY=${HAWK_API_KEY} -e NO_COLOR=true stackhawk/hawkscan'
            }
           }
         }

       }
    }
  }
}

